export interface Subject {
    image: string,
    title: string,
    overview: string,
    url: string
}
