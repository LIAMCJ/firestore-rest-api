import * as bodyParser from "body-parser";
import * as admin from 'firebase-admin';
import * as dotenv from 'dotenv';
import { Subject } from './interfaces';
import express from 'express';

dotenv.config();

//initialize express server
const app = express();

const serviceAccount = JSON.parse(process.env.serviceAccountKey);

//initialize firebase inorder to access its services
admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://subjects-database.firebaseio.com"
});

app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());
app.use(express.json());

//initialize the database and the collection 
const db = admin.firestore();
const collectionName = 'test';

app.get('/subjects', async (req, res) => {
    try {
        const subjectQuerySnapshot = await db.collection(collectionName).get();
        const subjectsArray: any[] = [];
        subjectQuerySnapshot.forEach(
            (doc)=>{
                subjectsArray.push({
                    id: doc.id,
                    data:doc.data()
            });
            }
        );
        res.status(200).json(subjectsArray);
    } catch (error) {
        res.status(500).send(error);
    }
});

app.post('/subjects', async (req, res) => {
    try {
        const subject:Subject = {
                image: req.body['image'],
                title: req.body['title'],
                overview: req.body['overview'],
                url:req.body['url']
        }

        const newDoc = await db.collection(collectionName).doc(subject.title).set(subject);
        res.status(201).send(`Created a new subject ${subject.title}`);
    } catch (error) {
        res.status(400).send(`Subject should contain id, image, title, overview, and url`)
    }
});

const PORT = process.env.PORT || 3000;

app.listen(PORT, () => console.log(`Server Started On Port ${PORT}`));

module.exports = app;