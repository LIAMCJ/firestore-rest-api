// Import the dependencies for testing
import chai from 'chai';
import chaiHttp from 'chai-http';

let app = require('../index');
chai.use(chaiHttp);

describe("GET /subjects", () => {
    // Test to get all subjects record
    it("should get all subjects", (done) => {
         chai.request(app)
             .get('/subjects')
             .end((err, res) => {
             	chai.expect(res).to.have.status(200);
             	chai.expect(res.body).to.be.a('array');
                done();
              });
     }); 
});